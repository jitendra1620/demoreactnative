import {useNavigation} from '@react-navigation/native';
import {
  SafeAreaView,
  StyleSheet,
  RefreshControl,
  TouchableWithoutFeedback,
  View,
  Text,
  Image,
  ActivityIndicator,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {FlatList} from 'react-native-gesture-handler';
import {themeColor, themeColorAlpha2} from '../Styles/Color';
import {Result} from '../Data Models/ArrSongsDM';
const axios = require('axios');

export default function SongsListScreen() {
  const navigation = useNavigation();
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [isPushLoading, setIsPushLoading] = useState<boolean>(false);
  const [arrSongs, arrSongsSet] = useState<Result[]>([]);
  useEffect(() => {
    callWebService();
  }, []);
  useEffect(() => {
    console.log(arrSongs, 'useEffect');
  }, [arrSongs]);
  const callWebService = async () => {
    {
      try {
        const response = await axios.get(
          'https://itunes.apple.com/search?term=Michael+jackson',
        );
        setIsPushLoading(false);
        setIsLoading(false);
        if (response?.status == 200) {
          arrSongsSet(response?.data?.results ?? []);
        }
      } catch (error) {
        setIsPushLoading(false);
        setIsLoading(false);
        console.error(error);
      }
    }
  };

  return (
    <SafeAreaView style={styles.safeArea_AppSplashScreen}>
      <FlatList
        refreshControl={
          <RefreshControl
            // colors={['#9Bd35A', '#689F38']}
            refreshing={isPushLoading}
            onRefresh={() => {
              setIsPushLoading(true);
              callWebService();
            }}
          />
        }
        style={{flex: 1}}
        data={arrSongs}
        renderItem={({item, index}: {item: Result; index: number}) => (
          <TouchableWithoutFeedback
            onPress={() => {
              navigation.navigate('SongsDetailScreen', {
                songDetails: item,
              });
            }}>
            <View style={styles.flatListCard}>
              <Image
                source={{
                  uri: item.artworkUrl100 ?? '',
                }}
                resizeMode={'contain'}
                style={styles.appLogo}></Image>
              <View style={styles.viewCardSubView}>
                <View style={{}}>
                  <Text
                    style={{
                      fontWeight: '900',
                      color: themeColor,
                    }}
                    numberOfLines={0}>
                    {item.collectionName}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                  }}>
                  <Text style={{marginRight: 15}}>{item.artistName}</Text>
                  <Text>
                    {((item.trackTimeMillis ?? 0) / 60000).toFixed(2) + ' m'}
                  </Text>
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>
        )}
        keyExtractor={(item, index) => index.toString()}
      />
      <ActivityIndicator
        animating={isLoading}
        color={themeColor}
        style={{position: 'absolute', alignSelf: 'center'}}
        size="large"
      />
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  safeArea_AppSplashScreen: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
  },
  appLogo: {
    height: 80,
    width: '20%',
    resizeMode: 'contain',
    alignSelf: 'center',
    marginLeft: 10,
    borderRadius: 4,
    backgroundColor: 'red',
    // width: '100%',
    // resizeMode: 'cover',
  },
  flatListCard: {
    height: 100,
    // height: 100,
    elevation: 1,
    backgroundColor: themeColorAlpha2,
    borderRadius: 4,
    margin: 2,
    shadowColor: 'rgba(0, 0, 0, 0.1)',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowRadius: 7,
    shadowOpacity: 0.4,
    flexDirection: 'row',
  },
  viewCardSubView: {
    flex: 1,
    margin: 20,
    marginTop: 20,
    marginBottom: 20,
    justifyContent: 'space-between',
    alignSelf: 'stretch',

    flexDirection: 'column',
  },
});
