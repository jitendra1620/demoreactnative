import {CommonActions, useNavigation} from '@react-navigation/native';
import {Image, SafeAreaView, StyleSheet} from 'react-native';
import React, {useEffect, useState} from 'react';
import {appLogon} from '../Images';
import {themeColorAlpha2} from '../Styles/Color';

export default function AppSplashScreen() {
  const navigation = useNavigation();
  useEffect(() => {
    const intervalId = setInterval(() => {
      navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [{name: 'SongsListScreen'}],
        }),
      );
    }, 1000);

    return () => clearInterval(intervalId);
  }, []);
  return (
    <SafeAreaView style={styles.safeArea}>
      <Image source={appLogon} style={styles.appLogo}></Image>
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: themeColorAlpha2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  appLogo: {
    aspectRatio: 1,
    // width: '100%',
    // resizeMode: 'cover',
  },
});
