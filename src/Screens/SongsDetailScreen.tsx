import {useNavigation, useRoute} from '@react-navigation/native';
import {View, FlatList, SafeAreaView, StyleSheet, Text} from 'react-native';
import React, {useEffect, useState} from 'react';
import {Result} from '../Data Models/ArrSongsDM';
import {themeColor, themeColorAlpha2} from '../Styles/Color';
interface keyValueObj {
  key: string;
  value: string;
}
export default function SongsDetailScreen() {
  const navigation = useNavigation();
  const route = useRoute();
  const [songDetail] = useState<Result>(route.params?.songDetails);
  const [arrValue, arrValueSet] = useState<keyValueObj[]>([]);
  useEffect(() => {
    // console.log(songDetail, 'songDetail');
    {
      var tempArr: keyValueObj[] = new Array();
      Object.keys(songDetail).forEach(function (k) {
        var x: keyValueObj = {key: k, value: songDetail[k]};
        tempArr.push(x);
      });
      arrValueSet(tempArr);
    }
  }, []);
  const AddView = () => {
    return <View></View>;
  };
  return (
    <SafeAreaView style={styles.safeArea_AppSplashScreen}>
      <FlatList
        style={{flex: 1}}
        data={arrValue}
        renderItem={({item, index}: {item: keyValueObj; index: number}) => (
          <View style={styles.flatListCard}>
            <Text style={styles.textView}>{item.key}</Text>
            <Text style={styles.textView}>{item.value}</Text>
          </View>
        )}
        keyExtractor={(item, index) => index.toString()}
      />
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  safeArea_AppSplashScreen: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
  },
  flatListCard: {
    flexDirection: 'row',
    margin: 2,
    backgroundColor: themeColorAlpha2,
    width: '98%',
    alignSelf: 'center',
    marginVertical: 4,
    padding: 8,
    alignContent: 'space-between',
    justifyContent: 'space-between',
  },
  textView: {width: '40%', fontSize: 17, color: themeColor},
});
