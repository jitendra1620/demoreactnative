export interface ArrSongsDM {
  resultCount?: number;
  results?: Result[];
}

export interface Result {
  wrapperType?: WrapperType;
  kind?: Kind;
  artistId?: number;
  collectionId?: number;
  trackId?: number;
  artistName?: string;
  collectionName?: string;
  trackName?: string;
  collectionCensoredName?: string;
  trackCensoredName?: string;
  artistViewUrl?: string;
  collectionViewUrl?: string;
  trackViewUrl?: string;
  previewUrl?: string;
  artworkUrl30?: string;
  artworkUrl60?: string;
  artworkUrl100?: string;
  collectionPrice?: number;
  trackPrice?: number;
  releaseDate?: Date;
  collectionExplicitness?: Explicitness;
  trackExplicitness?: Explicitness;
  discCount?: number;
  discNumber?: number;
  trackCount?: number;
  trackNumber?: number;
  trackTimeMillis?: number;
  country?: Country;
  currency?: Currency;
  primaryGenreName?: string;
  contentAdvisoryRating?: string;
  isStreamable?: boolean;
  collectionArtistId?: number;
  collectionArtistName?: string;
  collectionArtistViewUrl?: string;
  trackRentalPrice?: number;
  collectionHdPrice?: number;
  trackHdPrice?: number;
  trackHdRentalPrice?: number;
  longDescription?: string;
  hasITunesExtras?: boolean;
  shortDescription?: string;
  copyright?: string;
  description?: string;
  amgArtistId?: number;
}

export enum Explicitness {
  Explicit = 'explicit',
  NotExplicit = 'notExplicit',
}

export enum Country {
  Usa = 'USA',
}

export enum Currency {
  Usd = 'USD',
}

export enum Kind {
  FeatureMovie = 'feature-movie',
  Song = 'song',
}

export enum WrapperType {
  Audiobook = 'audiobook',
  Track = 'track',
}
