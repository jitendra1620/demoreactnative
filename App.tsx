/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import SplashScreen from './src/Screens/SplashScreen';
import SongsListScreen from './src/Screens/SongsListScreen';
import SongsDetailScreen from './src/Screens/SongsDetailScreen';
import {themeColor} from './src/Styles/Color';

const Stack = createStackNavigator();
function AppNavigationStack() {
  return (
    <Stack.Navigator
      initialRouteName="SplashScreen"
      screenOptions={{
        headerStyle: {
          backgroundColor: themeColor,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
      }}>
      <Stack.Screen
        name="SplashScreen"
        component={SplashScreen}
        options={{title: '', headerShown: false}}></Stack.Screen>
      <Stack.Screen
        name="SongsListScreen"
        component={SongsListScreen}
        options={{title: 'Songs', headerShown: true}}
      />
      <Stack.Screen
        name="SongsDetailScreen"
        component={SongsDetailScreen}
        options={{title: 'Songs', headerShown: true}}
      />
    </Stack.Navigator>
  );
}

const App = () => {
  return (
    <NavigationContainer>
      <AppNavigationStack />
    </NavigationContainer>
  );
};

export default App;
